from imageai.Prediction import ImagePrediction
import os
import sys
import base64
import math
import time


def detectProduct():
    execution_path = os.getcwd()
    prediction = ImagePrediction()
    prediction.setModelTypeAsResNet()

    encodedMedia = sys.argv[1]
    imgdata = base64.b64decode(encodedMedia)

    filename = f'{time.time()}.jpg'
    with open(filename, 'wb') as f:
        f.write(imgdata)

    model_path = "./objectDetection/models/resnet50_weights_tf_dim_ordering_tf_kernels.h5"
    #special_model_path = "./models/resnet50_weights_tf_dim_ordering_tf_kernels.h5"
    input_path = filename

    prediction.setModelPath(model_path)
    prediction.loadModel()
    try:
        predictions, percentage_probabilities = prediction.predictImage(
            input_path, result_count=1)
        os.remove(filename)
        if math.floor(percentage_probabilities[0]) > 85:
            print(f'{str(predictions[0])}')
    except (IOError, OSError, ValueError) as err:
        os.remove(filename)
        print(f'Error: {err}')


    # for index in range(len(predictions)):
    #    print(predictions[index], " : ", percentage_probabilities[index])
detectProduct()
