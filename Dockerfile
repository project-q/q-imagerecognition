FROM nikolaik/python-nodejs:python3.7-nodejs10

RUN pip install tensorflow keras opencv-python
RUN pip install imageai

WORKDIR /usr/src/app
COPY package*.json ./
RUN npm install

COPY . .
EXPOSE 3000
CMD ["node", "src/index.js"]