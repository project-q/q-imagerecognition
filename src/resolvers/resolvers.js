//var execSync = require('exec-sync');

module.exports = {
    Mutation: {
        detectProduct: async (_parent, args, _context, _info) => {

            const { encodedMedia } = args
            var { product, message } = await detectProduct(encodedMedia)
            return { name: product, message }
        }
    }
}



async function detectProduct(encodedImage) {

    const execSync = require('child_process').execSync;

    var code = await execSync(`python3 ./objectDetection/firstPrediction.py ${encodedImage}`);

    let product = code.toString('utf8').trim()
    //console.log(code.toString('utf8'))
    if (product.length == 0) {
        return { product: "", message: 'nothing detected' }
    } else {
        if (product.includes('Error')) {
            return { product: "", message: "invalid image" }
        } else {
            return { product, message: 'detected' }
        }
    }
}
